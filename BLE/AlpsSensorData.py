import pexpect
import time
import datetime
import os
import sys
NOF_REMAINING_RETRY = 3
while True:
    try:
        print("entered")
        child = pexpect.spawn("sudo bluetoothctl", timeout=30)
        child.sendline("teamsa123")
        print("spawned")
        child.sendline("scan on")
        print("sending")
        child.expect("0x0272", timeout=30)
        print("connected")
    except pexpect.TIMEOUT:
        NOF_REMAINING_RETRY = NOF_REMAINING_RETRY-1
        if (NOF_REMAINING_RETRY>0):
          print ("timeout, retry...")
          continue
        else:
          print ("timeout, giving up.")
          time.sleep(2)
          print("FAILED!")
          sys.exit(-1)
          break
    else:
        while True:
            ts = time.time()
            st = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
            day = datetime.date.today().strftime("%d/%m/%Y")
            child.readline()
            line = (child.before[117:120]).decode('utf-8')
            line = line.lstrip("272")
            data = line.strip("x")
            data = data.strip("-")
            file = open("Logg.csv", "a")
            if (os.path.getsize("Logg.csv")==0):
                print(end='')
            file.write(data)
            file = open("Logg.csv", "r+")
            res = file.read(34)
            if(res[0:2]!="00"):
                os.remove("Logg.csv")
            if(len(res)==34):
                rawacc_x = int((res[4:6] + res[2:4]), 16)
                if(rawacc_x & 0x8000) == 0x8000:
                    rawaccs_x = -( (rawacc_x ^ 0xffff) + 1)
                    acc_x = rawaccs_x/4096
                else:
                    acc_x = rawacc_x/4096
                rawacc_y = int((res[8:10] + res[6:8]), 16)
                if(rawacc_y & 0x8000) == 0x8000:
                    rawaccs_y = -( (rawacc_y ^ 0xffff) + 1)
                    acc_y = rawaccs_y/4096
                else:
                    acc_y = rawacc_y/4096
                rawacc_z = int((res[12:14] + res[10:12]), 16)
                if(rawacc_z & 0x8000) == 0x8000:
                    rawaccs_z = -( (rawacc_z ^ 0xffff) + 1)
                    acc_z = rawaccs_z/4096
                else:
                    acc_z = rawacc_z/4096
                rawpes = int((res[16:18] + res[14:16]), 16)
                pes = (rawpes*860/65535)+250
                rawhum = int((res[20:22] + res[18:20]), 16)
                hum = (rawhum-896)/64
                rawtemp = int((res[24:26] + res[22:24]), 16)
                temp = (rawtemp-2096)/50
                rawuv = int((res[28:30] + res[26:28]), 16)
                uv = rawuv/(100*0.388)
                rawamb = int((res[32:34] + res[30:32]), 16)
                amb = rawamb/(0.05*0.928)
                file = open("Log_Alps.csv", "a")
                if (os.path.getsize("Log_Alps.csv")==0):
                    file.write("Time\tDate\tAcc_X[G]\tAcc_Y[G]\tAcc_Z[G]\tPressure[hPa]\tHumidity[%RH]\tTemperature[degC]\tUV[mW/cm^2]\tAmbient[Lx]\n")
                file.write(str(st))
                file.write("\t")
                file.write(str(day))
                file.write("\t")
                file.write(str("%.5f" % acc_x))
                file.write("\t")
                file.write(str("%.5f" % acc_y))
                file.write("\t")
                file.write(str("%.5f" % acc_z))
                file.write("\t")
                file.write(str("%.2f" % pes))
                file.write("\t")
                file.write(str("%.2f" % hum))
                file.write("\t")
                file.write(str("%.2f" % temp))
                file.write("\t")
                file.write(str("%.2f" % uv))
                file.write("\t")
                file.write(str("%.2f" % amb))
                file.write("\n")
                print("%.5f" % acc_x, end=' ')
                print("%.5f" % acc_y, end=' ')
                print("%.5f" % acc_z, end=' ')
                print("%.2f" % pes, end=' ')
                print("%.2f" % hum, end=' ')
                print("%.2f" % temp, end=' ')
                print("%.2f" % uv, end=' ')
                print("%.2f" % amb)
                os.remove("Logg.csv")
                time.sleep(5) 
                break
