#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

# Copyright (c)
# All rights reserved.

# Contributors:
#    Anushri T

# The script handles access to InfluxDB


from influxdb import InfluxDBClient

import sys
sys.path.insert(0, '/home/demokit-iot/Documents/BLE')
#from .ZigbeeCode.Auth.credentials import Constant
from Auth.credentials import Constant

'''TO DO : Add exceptions, logs, method level documentation'''
         
#Values to be moved to a separate configuration file or handled using GitLab

#DB_PORT = 8086
#DB_HOST = 'localhost'
#Admin should not be used ideally
#DB_USERNAME = 'admin'
#DB_PASSWORD = 'admin''''

def insert_data_to_db(client, db_name, value_list):
    print("Inside DB Utils")
    print(value_list)
    client.switch_database(db_name)
    client.write_points(value_list)
    print("inserted")


#defualt values should be used with caution as functions are treated as firs-class objects : https://stackoverflow.com/questions/1132941/least-astonishment-and-the-mutable-default-argument
def generate_db_client(host=Constant.DB_HOST, port=Constant.DB_PORT, username=Constant.DB_USERNAME, password=Constant.DB_PASSWORD):
    #TO DO: Handle Exceptions in establishing connection and creating client 
    print("Hello Create client")
    client = InfluxDBClient(host, port, username, password)
    print(client)
    return client

