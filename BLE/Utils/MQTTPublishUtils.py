

#Handle Exceptions
import paho.mqtt.publish as publish


#from Auth.credentials import Constant

import sys
sys.path.insert(0, '/home/anagha/Desktop/ZigbeeCode')
#from .ZigbeeCode.Auth.credentials import Constant
from Auth.credentials import Constant

def publish_to_mqtt(host, topic_name, payload_data):
    """
      Publishes data to a given MQTT Topic; throws MQTTException

      Parameters:
      host (string) : hostname of the MQTT broker
      topicname(string) : name of the MQTT Topic
      payload_data(string) : data to be published to MQTT Topic

      """
    
    auth = {
            'username':Constant.MQTT_USERNAME, #"iotdemo",
            'password':Constant.MQTT_PASSWORD #"demo@iot"
    }
                                                                                                                                                                                                                                                
    publish.single(topic=topic_name, payload=payload_data, hostname=host, auth=auth)
    #print(auth)
    print("published")



