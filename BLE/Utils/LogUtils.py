#Priority: Make logger singleton
#Complete documentation

from logging.handlers import RotatingFileHandler
import logging


LOG_FILE_NAME = "ReadAndPublishToMQTT.log"
LOG_MAX_BYTES = 10000
LOG_BACK_UP_COUNT = 2

def get_logger(logfile=LOG_FILE_NAME, max_bytes=LOG_MAX_BYTES, back_up_count=LOG_BACK_UP_COUNT):
    formatter = logging.Formatter("%(asctime)s;%(levelname)s;%(message)s",
                              "%Y-%m-%d %H:%M:%S")
    logger = logging.getLogger('sub_logger')
    logger.setLevel(logging.DEBUG)
    handler = RotatingFileHandler(logfile, max_bytes, back_up_count)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger

