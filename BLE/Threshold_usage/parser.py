import json

import glob
import csv
import pandas as pd



def parse_param(oldFile) :


    json_data = {}
    df = pd.DataFrame()
    #data = []
    #fields = ['Sensor_id','Parameter','lower_threshold','middle_threshold','upper_threshold']
    file_location = '/home/demokit-iot/Documents/BLE/Threshold_usage/threshold_data.csv'
    d = []
    #print("dfgdfyd")

    #file_path = '/home/anagha/Downloads/org1/dashboards/monitoring-dashboard.json'
    #file_path = '/home/anagha/Downloads/org1/dashboards/ripples-iot-pte-ltd.json'

    #datastore = {}
    #Read JSON data into the datastore variable
    '''if filename:
        with open(filename, 'r') as f:
            #print(f)
            datastore = json.loads(f)
    '''
    print(str(oldFile))

    with open(oldFile) as f:
        data = json.loads(f.read())
    #datastore = json.loads('/home/anagha/Downloads/org1/dashboards/monitoring-dashboard.json')
    #print(data)

    n = 0
    for each in data['dashboard']['panels']:
        #data1 = list(each.keys())
        #data2 = list(each.values())
        #print(data1)
        #print(data2)
        #selected = 'thresholds'
        #print(datas["thresholds"])
        #datas = each.items()
        #print(n)
        #print(list(datas)[0])
        #n=n+1

        #print(len(each.items()))
        #print(type(each.items()))


        lower_threshold = 0
        upper_threshold = 0
        middle_threshold = 0
        Sensor_id = ''
        Parameter = ''
        n=1

        for key, value in each.items():
            #print(key, '', value)
            #if key == 'thresholds' or key == 'targets':
            if key == 'thresholds' :
                #print(value)
                if len(value) != 0 :
                    #print(value)
                    substring = ","
                    count = value.count(substring)
                    #print(count)
                    if count == 1 :
                        lower_threshold,upper_threshold = value.split(',')
                        middle_threshold = ''
                    elif count == 2 : 
                        lower_threshold,middle_threshold,upper_threshold = value.split(',')
                    #upper_threshold = value
                    #print(lower_threshold)
                else:
                    lower_threshold = "No threshold"
                    middle_threshold = "No threshold"
                    upper_threshold = "No threshold"   

                #print(lower_threshold, "+" ,upper_threshold)             
                #print(key, '',value)
                #print(type(value))
            if key == 'targets' :
                Sensor_id = value[0]['measurement']
                parameter_1 = (value[0]['select'][0][0]['params'])
                parameter_temp = str(parameter_1).replace("['","")
                Parameter = parameter_temp.replace("']","")
                #print(value[0]['measurement'])
                #print(str(value[0]['select'][0][0]['params']))

            #print(lower_threshold)
        if lower_threshold != 0 :
            if lower_threshold != 'No threshold' :
                #print(lower_threshold, "+" ,middle_threshold, "+" ,upper_threshold, "+" ,Sensor_id, "+" ,Parameter)
                #print(n)
                data = {}
                data['lower_threshold'] = lower_threshold
                data['middle_threshold'] = middle_threshold
                data['upper_threshold'] = upper_threshold
                data['Sensor_id'] = Sensor_id
                data['Parameter'] = Parameter

                d.append((Sensor_id,Parameter,lower_threshold,middle_threshold,upper_threshold))
    df = pd.DataFrame(d, columns=('Sensor_id','Parameter','lower_threshold','middle_threshold','upper_threshold'))
    with open(file_location, 'a') as f:
        #print(n)
        print(df)
        df.to_csv(f,  sep=",", encoding="utf-8", index=False)
                #print(data['Parameter'])
                #json_data = json.dumps(data)
                #json_data = data
                #print(json_data)
                #print((each.items())['targets'])



        n = n+1
    #print(data)
    return(json_data) 
    #print(data.dashboard.panels[2].thresholds)



for filePath in glob.glob("/home/demokit-iot/Documents/BLE/Threshold_usage/org1/dashboards/*.json"):
    json_data_result = {}
    #with open(filePath,"rb") as oldFile:
    #print(filePath)
    json_data_result = parse_param(filePath)
    #print("hello")
    #print(json_data_result)
    # print(parse_param(filePath))
    #return(parse_param(filePath))
