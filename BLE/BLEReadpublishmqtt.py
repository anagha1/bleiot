import re
import signal
import logging
from influxdb import InfluxDBClient
from logging.handlers import RotatingFileHandler
import paho.mqtt.publish as publish
from Utils.LogUtils import *
#from Utils.CoordinatorUtils import *
from Utils.MQTTPublishUtils import *
import json
import os
import sys
sys.path.insert(0, '/home/demokit-iot/Documents/BLE')
#from .ZigbeeCode.Auth.credentials import Constant
from Auth.credentials import Constant

import pexpect

LOG_FILE_NAME = "ReadAndPublishToMQTT.log"
LOG_MAX_BYTES = 10000
LOG_BACK_UP_COUNT = 2
#MQTT_HOST = "localhost"
#MQTT_TOPIC = "test"
#DEVICE_FILE_PATH = "/home/openhabian/Desktop/Demo/CoordinatorConfig"
#COORDINATOR_FILE_NAME = "CoordinatorConfig"


def read_data_and_publish_to_mqtt():
    """
    Reads data from Zigbee devices provided in the configuration files, and publishes it to a MQTT Topic
    """

    '''Read configuration details of the coordinator'''
    try:
        print("try to connect")
        child = connect_to_ble()
        print("connected")
        #print(child)
        #sensor_data = "Hello"
    except Exception as e:
        logger.exception("Issue establishing connection with BLE Device")
        raise Exception("Issue establishing connection with BLE Device", e)

    while (True):
        sensor_data = read_from_ble(child)
        #sensor_data = "Hello"
        print(sensor_data)
        try:
            print("Hi")
            publish_to_mqtt(Constant.MQTT_HOST, Constant.MQTT_TOPIC, str(sensor_data))
        except Exception as e:
            #Rewrite to catch specific exception
            logger.exception("Issue publishing to MQTT Topic")
            raise Exception("Issue publishing to MQTT Topic", e)



def connect_to_ble():
    """
    Establishes connection with the Zigbee Coordinator
    Parameters:
     file_name (string) : file containing coordinator configuration
    Return:
      xbee : Zigbee xbee Object

    """
    try:
        child = pexpect.spawn("bluetoothctl", timeout=None)
        child.expect("[bluetooth]", timeout=None)
        #child.sendline("power off")
        #child.sendline("power on")  
        child.sendline("scan on")
        #child.expect("Device ")
        #print(child)
        inp=child.readline()
        #print(inp)
        return child
    except Exception as e :
        '''logger.exception("Unable to read config or connect to Zigbee Device")
        raise Exception("Unable to read config or  connect to Zigbee Device", e)'''
        #check for timeout(check alps code)

def read_from_ble(child):
    """
    Return:
      device_date : dataframe from zigbee devices in the network

    """

    #---
    #execution time
    '''import time
    start = time.time()
    print("start")
    print(start)'''
    #---


    #Handle Exceptions
    #device_data = child.readline()
    '''print( type(device_data))
    print("device data")
    print(device_data)'''
    #child.expect("Device ", timeout=None)
    #print(child)
    #child.expect("ServiceData Value: ", timeout=300)
    inp=child.readline()
    '''print(child.readline())
    print(inp)'''
    return child.readline()


if __name__ == '__main__':
    #Revisit global
    global logger
    logger = get_logger(LOG_FILE_NAME, LOG_MAX_BYTES, LOG_BACK_UP_COUNT)

    read_data_and_publish_to_mqtt()
