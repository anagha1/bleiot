#Required only if default parameters will be specified
#Revisit the exact type returned by ZigBee()

import os
import sys
import serial
from Utils.LogUtils import *
from xbee import ZigBee
import time
import pexpect

FILE_NAME = "CoordinatorConfig"

LOG_FILE_NAME = "ReadAndPublishToMQTT.log"
LOG_MAX_BYTES = 10000
LOG_BACK_UP_COUNT = 2

global logger
logger = get_logger(LOG_FILE_NAME, LOG_MAX_BYTES, LOG_BACK_UP_COUNT)


def connect_to_coordinator(file_name):
    """
    Establishes connection with the Zigbee Coordinator
    Parameters:
     file_name (string) : file containing coordinator configuration
    Return:
      xbee : Zigbee xbee Object

    """
    try:
        '''Read configuration details of the coordinator'''
        #Could be revisited https://stackoverflow.com/questions/4060221/how-to-reliably-open-a-file-in-the-same-directory-as-a-python-script'''
        device_config_file = open(os.path.join(sys.path[0], file_name), "r")
        PORT = device_config_file.readline().rstrip().split(':')[1]
        BAUD_RATE = device_config_file.readline().rstrip().split(':')[1]

        '''data = pexpect.spawn('sudo chmod 777 ' + PORT)
        if data.expect(['(?i)password.*']) == 0:
           data.sendline('teamsa123')
           time.sleep(2)
           data.expect(pexpect.EOF,5)
        else:
           raise AssertionError("failed")'''

        ''' Open serial port'''
        ser_port = serial.Serial(PORT, BAUD_RATE)
        xbee = ZigBee(ser_port, escaped=True)
        return xbee
        #FileNotFoundError and SerialException both can be caught using IOError 
    except FileNotFoundError as e:
        logger.exception("Issue reading zigbee coordinator details from config files")
        raise Exception("Issue reading zigbee coordinator details from config files.", e)
    except serial.SerialException as e:
        logger.exception("Issue opening serial port and connecting to Zigbee device")
        raise Exception("Issue opening serial port and connecting to Zigbee device", e)
    except Exception as e :
        logger.exception("Unable to read config or connect to Zigbee Device")
        raise Exception("Unable to read config or  connect to Zigbee Device", e)

def read_from_coordinator(xbee):
    """
    Reads data received by the  Zigbee Coordinator
    Parameters:
     xbee (Zigbee Xbee Object) : hostname of the MQTT broker
    Return:
      device_date : dataframe from zigbee devices in the network

    """
    #Handle Exceptions
    device_data = xbee.wait_read_frame()
    print( type(device_data))
    return device_data    



