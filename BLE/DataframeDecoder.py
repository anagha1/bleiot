import os
import sys
import ast
import re
import sys
from Utils.Status_Utils import *
sys.path.insert(0, '/home/demokit-iot/Documents/BLE')
#TO DO : More code refactoring to be done; split to more modules if possible
SENSOR_DETAILS_FILE_NAME = "SensorDetails"


def populate_sensor_details(sensor_details_file_name):

    '''Read sensor detail and load it to a dictionary'''
    sensor_file = open(os.path.join(sys.path[0], sensor_details_file_name), 'r')
    sensor_details_dict_result = {}
    #print(sensor_file)
    
    for line in sensor_file:
         #print(line)
         (sensor_add, sensor_conf) = line.split()
         #print(sensor_add)
         #print(sensor_conf)
         sensor_details_dict_result[sensor_add] = sensor_conf
    '''print("sensor_details_dict_result")
    print(sensor_details_dict_result)'''
    return sensor_details_dict_result


def read_sensor_data(device_data_frame,sensor_details_dict_param,data_decode_dict):
    Measured_power = -45
    device_reading_dict = {}
    sensor_data_temp = {}
    sensor_value_list = []
    #data_decode_dict = {}
    print("device_data_frame")
    print(device_data_frame)
    print(type(device_data_frame))

    device_dataframe_result = re.sub(' +', ' ', device_data_frame)
    print("device_dataframe_result")
    print(device_dataframe_result)

    global sensor_details_dict
    sensor_details_dict = sensor_details_dict_param

    mac_id = device_data_frame[90:107]
    print(mac_id)
    #Return if source_addr is not in config file
    if mac_id not in sensor_details_dict:
        #print("not present")
        return []
    
    sensor_details_str = sensor_details_dict[mac_id]
    print("sensor_details_str")
    print(type(sensor_details_str))
    print("sensor_details_str")
    #print(type(device_data_frame))
    #print(device_data_frame)
    #mac_id = (child.before[52:69]).decode('utf-8')



    if mac_id in data_decode_dict.keys() :
        initial_value = data_decode_dict[mac_id] 
    else :
        initial_value = ""

    n = 0
    if(sensor_details_str =="Puck_ID"):
        puck_rssi = (device_data_frame[114:117])
        print(puck_rssi)
        print(puck_rssi[0])
        print("entered fe")
        if(puck_rssi[0] == "-"):
           print (mac_id)
           print (puck_rssi)
           puck_dist = 10 ** ((Measured_power - int(puck_rssi))/(10 * 2))
           device_reading_dict["distance"] = float(puck_dist)
           device_reading_dict["rssi_value"] = float(puck_rssi)
           sensor_data_temp["measurement"] = 'BLE' +str(mac_id).replace(':' , '_')
           sensor_data_temp["fields"] = device_reading_dict
        n = n + 1
    if(sensor_details_str == "Coin_T"):
        coin_t = (device_data_frame[127:131])
        puck_rssi = (device_data_frame[114:117])
        if(puck_rssi[0] == "-"):
           print (mac_id)
           print (puck_rssi)
           puck_dist = 10 ** ((Measured_power - int(puck_rssi))/(10 * 2))
           device_reading_dict["distance"] = float(puck_dist)
           device_reading_dict["rssi_value"] = float(puck_rssi)
           sensor_data_temp["measurement"] = 'BLERSSI' +str(mac_id).replace(':' , '_')
           sensor_data_temp["fields"] = device_reading_dict
        print(coin_t + "hii")
        if(coin_t[0:2]=="0x"):
            coin_t = coin_t.lstrip("0")
            coin_t = coin_t.lstrip("x")
            data_decode_dict[mac_id] = initial_value + coin_t
            print("data_decode_dict")
            print(data_decode_dict)
            print(data_decode_dict[mac_id])
            print(type(data_decode_dict[mac_id]))
            '''file = open("coin.csv", "a")
            if (os.path.getsize("coin.csv")==0):
                print(end='')
            file.write(coin_t)
            file = open("coin.csv", "r")
            res = (file.read())'''
            res = data_decode_dict[mac_id]
            print("res")
            print(res)
            if(len(res)==4):
                rawtemp1 = int((res[2:4] + res[0:2]), 16)
                rawtemp = int(str(rawtemp1)[:4])
                print("rawtemp1")
                print(rawtemp1)
                print("rawtemp")
                print(rawtemp)
                temp = ((rawtemp/100) - 2)
                print(temp, end='')
                print("°C")
                #os.remove("coin.csv")
                device_reading_dict["Temperature"] = float(temp)
                sensor_data_temp["measurement"] = 'BLE' +str(mac_id).replace(':' , '_')
                sensor_data_temp["fields"] = device_reading_dict
                data_decode_dict[mac_id] = ""
    if(sensor_details_str == "Coin_MOV"):     
        mov_acc = (device_data_frame[127:131])
        puck_rssi = (device_data_frame[114:117])
        #print(mov_acc)
        if(puck_rssi[0] == "-"):
           print (mac_id)
           print (puck_rssi)
           puck_dist = 10 ** ((Measured_power - int(puck_rssi))/(10 * 2))
           device_reading_dict["distance"] = float(puck_dist)
           device_reading_dict["rssi_value"] = float(puck_rssi)
           sensor_data_temp["measurement"] = 'BLERSSI' +str(mac_id).replace(':' , '_')
           sensor_data_temp["fields"] = device_reading_dict
        print(mov_acc + "hhh")
        if(mov_acc[0:2]=="0x"):
            mov_acc = mov_acc.lstrip("0")
            mov_acc = mov_acc.lstrip("x")
            data_decode_dict[mac_id] = initial_value + mov_acc
            print("data_decode_dict")
            print(data_decode_dict)
            print(data_decode_dict[mac_id])
            print(type(data_decode_dict[mac_id]))
            '''file = open("mov.csv", "a")
            if (os.path.getsize("mov.csv")==0):
                print(end='')
            file.write(mov_acc)
            file = open("mov.csv", "r")
            res = (file.read())'''
            res = data_decode_dict[mac_id]
            print("res")
            print(res)
            if(len(res)==12):              
                rawacc_x = int((res[2:4] + res[0:2]), 16)
                if(rawacc_x & 0x8000) == 0x8000:
                    rawaccs_x = -( (rawacc_x ^ 0xffff) + 1)
                    acc_x = rawaccs_x
                else:
                    acc_x = rawacc_x
                rawacc_y = int((res[6:8] + res[4:6]), 16)
                if(rawacc_y & 0x8000) == 0x8000:
                    rawaccs_y = -( (rawacc_y ^ 0xffff) + 1)
                    acc_y = rawaccs_y
                else:
                    acc_y = rawacc_y
                rawacc_z = int((res[10:12] + res[8:10]), 16)
                if(rawacc_z & 0x8000) == 0x8000:
                    rawaccs_z = -( (rawacc_z ^ 0xffff) + 1)
                    acc_z = rawaccs_z
                else:
                    acc_z = rawacc_z
                print("%.2f" % acc_x,end=' ')
                print("%.2f" % acc_y,end=' ')
                print("%.2f" % acc_z,end=' ')
                print(end='\n')
                #os.remove("mov.csv")
                device_reading_dict["XAxis"] = float(acc_x)
                device_reading_dict["YAxis"] = float(acc_y)
                device_reading_dict["ZAxis"] = float(acc_z)
                sensor_data_temp["measurement"] = 'BLE' +str(mac_id).replace(':' , '_')
                sensor_data_temp["fields"] = device_reading_dict
                print(device_reading_dict)
                data_decode_dict[mac_id] = ""
                #print("hi" + data_decode_dict[mac_id] + "hi")


    print("puck_count" + str(n))
    print("sensor_data_temp")
    print(sensor_data_temp)
    if (len(sensor_data_temp)) != 0 :
    	check_status(sensor_data_temp)

    sensor_value_list.append(sensor_data_temp)
    print(sensor_value_list)
    return sensor_value_list
#child.close()


