import datetime
from datetime import timezone
from dateutil import parser
from datetime import datetime
from datetime import timedelta
import pytz
import time
import os
import csv
import pandas as pd
import sys
from Utils.DBUtils import *


sys.path.insert(0, '/home/demokit-iot/Documents/BLE')



#{'measurement': 'Z0013a200415b5f82', 'fields': {'XAxis': 0.37, 'YAxis': 0.02, 'ZAxis': 0.02}}


def check_status(dict_val):

    print("entered to check status")
    client = generate_db_client()
    device_reading_dict = {}
    device_reading_dict_tag = {}
    sensor_data_temp = {}
    sensor_values_dict = {}
    value_list = []
    flag = 1

    Sensor = dict_val['measurement']
    #print(dict_val['fields']['XAxis'])
    each = dict_val['fields']
    #print(dict_val['fields'].keys()[1])
    '''for key, value in each.items():
        print(key,value)'''

    #data = pd.read_csv("/home/anagha/Documents/ZigbeeCode/Threshold_usage/threshold_data.csv")
    with open("/home/demokit-iot/Documents/BLE/Threshold_usage/threshold_data.csv", 'r') as file:
        reader = csv.DictReader(file)
        list = []
        arr = []
        for row in reader:
            if row['Sensor_id'] == Sensor :
                #list.append(float(row['temperature']))
                print(row)
                for key, value in each.items():
                    if row['Parameter'] == key :
                        print(value)
                        print("threshold")
                        print(float(row['lower_threshold']))
                        #print(type(row['lower_threshold'])
                        if value < float(row['lower_threshold'])  or value > float(row['upper_threshold']) :
                            #print("Device failure")
                            #print(row['lower_threshold'])
                            #sensor_data_temp["measurement"] = "Failure"
                            #device_reading_dict_tag["Parameter"] = key
                            arr.append('Failure')
                        else :
                            #print("Device success")
                            #sensor_data_temp["measurement"] = "Success"
                            arr.append('success')

                        #device_reading_dict_tag["Parameter"] = key
                        #device_reading_dict["value"] = value

            
                        #device_reading_dict["OnOff"] = 1
            
        
        #print(value_list)
        #insert_data_to_db(client, "zigbeeDB", value_list)
        if 'Failure' in arr : 
            sensor_data_temp["measurement"] = "Failure"
            device_reading_dict_tag["Status"] = "Failure"
        else :
            sensor_data_temp["measurement"] = "Success"
            device_reading_dict_tag["Status"] = "Success"

        sensor_data_temp["tags"] = device_reading_dict_tag
        device_reading_dict["Sensor_id"] = Sensor
            
        sensor_data_temp["fields"] = device_reading_dict
        value_list.append(sensor_data_temp)
        print(arr)
    insert_data_to_db(client, "zigbeeDB", value_list)
    print("inserted")



  





#dict_val = {'measurement': 'Z0013a200415b5f82', 'fields': {'XAxis': 0.37, 'YAxis': 0.02, 'ZAxis': 0.02}}
#dict_val = {'measurement': 'Z0013a200415b5f82', 'fields': {'XAxis': 0.37, 'YAxis': 0.37, 'ZAxis': 0.37}}
#dict_val = {'measurement': 'Z0013a200415b5f3e', 'fields': {'Temperature': 27.0, 'Humidity': 35.0, 'Noise': 0.46}}
#print(dict_val)
#print(type(dict_val))

#check_status(dict_val)
