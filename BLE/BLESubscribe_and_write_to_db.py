#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

# Copyright (c)
# All rights reserved.

# Contributors:
#    Anushri T

# The script reads encoded Zigbee frames from MQTT, decodes the same and writes to influxDB


from asyncio import log
import paho.mqtt.client as mqtt
from xbee import ZigBee
import serial
import json
import re
import os
import sys
import signal
import logging
from influxdb import InfluxDBClient
import ast
from threading import Thread
from logging.handlers import RotatingFileHandler
from Utils.DBUtils import *
from Utils.LogUtils import *
from DataframeDecoder import *

import sys
sys.path.insert(0, '/home/demokit-iot/Documents/BLE')
#from .ZigbeeCode.Auth.credentials import Constant
from Auth.credentials import Constant

LOG_FILE_NAME = 'subscribeAndWriteToDB.log'
#DEVICE_CONFIG_FILE_NAME = "CoordinatorConfig"
#SENSOR_DETAILS_FILE_NAME = "SensorDetails"
#BURSTY_SENSOR_DETAILS_FILE_NAME = "BurstySensorDetails"
'''DB_PORT = 8086
DB_HOST = 'localhost'
USERNAME = 'master'
PASSWORD = 'demo@iot'
DB_NAME = 'zigbeeDB'
MQTT_HOST = 'localhost'
MQTT_PORT = 1883
MQTT_TOPIC = 'test'
'''

global data_decode_dict
data_decode_dict = {}

def on_connect(mqttc, obj, flags, rc):
    logger.info("rc: " + str(rc))


def on_message(mqttc, obj, msg):
    print("Inside on_message!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"); 
    #print(msg.topic + " " + str(msg.qos) + " " + msg.payload.decode("utf-8"))
    #call the function to decode and write the same to influxdb
    print(msg.payload)
    print(msg.payload.decode("utf-8"))
    process_sensor_readings_thread =  Thread(target=read_sensor_data_and_write_to_db, args=(msg.payload.decode("utf-8"),))
    #process_sensor_readings_thread =  Thread(target=read_sensor_data_and_write_to_db, args=(msg.payload))
    process_sensor_readings_thread.start()

def on_publish(mqttc, obj, mid):
    logger.info("mid: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    logger.info("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    logger.info(string)


def read_sensor_data_and_write_to_db(device_data_frame):
     
    '''sensor_details_dict_param =  populate_sensor_details(SENSOR_DETAILS_FILE_NAME)
    sensor_details_dict_burst_param =  populate_sensor_details(BURSTY_SENSOR_DETAILS_FILE_NAME)
    sensor_value_list = read_sensor_data(device_data_frame, sensor_details_dict_param, sensor_details_dict_burst_param)
    print(sensor_value_list)'''
    #print(type(device_data_frame))
    sensor_details_dict_param =  populate_sensor_details(SENSOR_DETAILS_FILE_NAME)
    sensor_value_list = read_sensor_data(device_data_frame,sensor_details_dict_param,data_decode_dict)
    print("sensor_value_listrrrr")
    print(sensor_value_list)
    value_puck = len(sensor_value_list)
    print("value_puck")
    print(value_puck)
    if(value_puck != 0) :
       print(sensor_value_list[0])
       print(len(sensor_value_list[0]))
       value_len = len(sensor_value_list[0])

       #Log all DB realted Exceptions and may be throw the exceptions
       '''if(sensor_value_list):
           client = generate_db_client(Constant.DB_HOST,Constant.DB_PORT, Constant.DB_USERNAME, Constant.DB_PASSWORD)
           insert_data_to_db(client, Constant.DB_NAME, sensor_value_list)'''

       if(value_len != 0):
           client = generate_db_client(Constant.DB_HOST,Constant.DB_PORT, Constant.DB_USERNAME, Constant.DB_PASSWORD)
           insert_data_to_db(client, Constant.DB_NAME, sensor_value_list)

           #---
           #execution time
           '''import time
           start = time.time()
           print("end")
           print(start)'''
           #---
       else : 
           print("pass")
           
           #---
           #execution time
           '''import time
           start = time.time()
           print("end")
           print(start)'''
           #---

    else :
        return []






# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.

# create formatter
global logger

#global data_decode_dict = {}

logger = get_logger()

#Rethink where to  call populate_details; ideally any issue here should be caught first
#populate_sensor_details()

#read_sensor_data_and_write_to_db("abc")
mqttc = mqtt.Client()
mqttc.username_pw_set(Constant.MQTT_USERNAME, Constant.MQTT_PASSWORD)
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe

# Uncomment to enable debug messages
mqttc.on_log = on_log
mqttc.connect(Constant.MQTT_HOST, Constant.MQTT_PORT, 60)
'''ADD AUTHENTICATION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''
mqttc.subscribe(Constant.MQTT_TOPIC, 0)


mqttc.loop_forever()

